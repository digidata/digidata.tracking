﻿
using System;
using System.IO;
using System.Threading;
using NUnit.Framework;

namespace DigiData.Tracking
{
    [TestFixture]
    public class Aspects_Tests
    {
        [Test]
        public void IsTransformedByPostSharp_AlwaysTrue()
        {
            Assert.That(TrackingUtilities.IsTransformedByPostSharp, Is.True);
        }
    }
}
