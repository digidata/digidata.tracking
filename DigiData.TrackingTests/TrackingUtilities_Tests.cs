﻿
using System;
using System.IO;
using System.Threading;
using NUnit.Framework;

namespace DigiData.Tracking
{
    [TestFixture]
    [Explicit]
    public class TrackingUtilities_Tests
    {
        [Test]
        public void TrackingSetUp_MustSetTrackingToLoading()
        {
            var tmp = Path.GetTempPath();
            TrackingConfig.ConfigPath = tmp;
            TrackingConfig.Version = "1";

            TrackingUtilities.TrackingSetUp("http://www.aefwerf23crfewrwcfcwf.com", "111222333", "", false, "", true);

            Assert.That(TrackingUtilities.IsTrackingLoading, Is.True);
        }

        [Test]
        public void TrackingSetUp_MustQueuePostStartConfig()
        {
            var tmp = Path.GetTempPath();
            TrackingConfig.ConfigPath = tmp;
            TrackingConfig.Version = "1";

            TrackingUtilities.TrackingSetUp("http://www.aefwerf23crfewrwcfcwf.com", "111222333", "", false, "", true);

            Assert.That(TrackingUtilities.HasQueuedEvents, Is.True);
        }

        [Test]
        public void TrackingSetUp_MustClearQueuedEventsAfterLoad()
        {
            var tmp = Path.GetTempPath();
            TrackingConfig.ConfigPath = tmp;
            TrackingConfig.Version = "1";

            TrackingUtilities.TrackingSetUp("http://www.aefwerf23crfewrwcfcwf.com", "111222333", "", false, "", true);

            Thread.Sleep(1000);

            Assert.That(TrackingUtilities.HasQueuedEvents, Is.False);
        }

        [Test]
        public void TrackEvent_MustClearQueuedEventAfterLoad()
        {
            var tmp = Path.GetTempPath();
            TrackingConfig.ConfigPath = tmp;
            TrackingConfig.Version = "1";

            TrackingUtilities.TrackingSetUp("http://www.aefwerf23crfewrwcfcwf.com", "111222333", "", false, "", true);
            TrackingUtilities.TrackEvent("TestEvent", 10);

            Thread.Sleep(1000);

            Assert.That(TrackingUtilities.HasQueuedEvents, Is.False);
        }
    }
}
