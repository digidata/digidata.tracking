﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiData.Tracking
{
    public static class TrackingConfig
    {
        private static string _version;
        /// <summary>
        /// The version of the application. This must be set before trying to start
        /// tracking. The value will not be updated in tracked events until tracking
        /// is stopped and then started again.
        /// </summary>
        public static string Version
        {
            get
            {
                if(String.IsNullOrEmpty(_version))
                    throw new TrackingConfigException("Version is not set. Please assign version before attempting to start tracking.");

                return _version;
            }

            set { _version = value; }
        }

        private static string _global_session_id;
        /// <summary>
        /// Set when there are multiple processes using the same configuration
        /// files. Name will be used as a global identifier for events triggered
        /// by the process. If not set and multiple sessions are enabled you must
        /// provide a session name when tracking events. The value will not be
        /// updated until tracking is stopped and then started again.
        /// </summary>
        public static string GlobalSessionID
        {
            get { return _global_session_id; }
            set
            {
                _global_session_id = value;

                IsTrackingSessionGlobal = !String.IsNullOrEmpty(_global_session_id);
            }
        }

        private static string _config_path;
        /// <summary>
        /// Where the tracking config files will be stored, usually the appdata
        /// location. This must be set before trying to start tracking. The value
        /// will not be updated in tracked events until tracking is stopped and
        /// then started again.
        /// </summary>
        public static string ConfigPath
        {
            get
            {
                if (String.IsNullOrEmpty(_config_path))
                    throw new TrackingConfigException("ConfigPath is not set. Please assign a path before attempting to start tracking.");

                return _config_path;
            }

            set { _config_path = value; }
        }

        public static bool IsTrackingSessionGlobal { get; private set; }

        private static bool _tracking_enabled = true;
        public static bool TrackingEnabled
        {
            get { return _tracking_enabled; }
            set { _tracking_enabled = value; }
        }
    }
}
