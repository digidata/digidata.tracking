﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiData.Tracking
{
    /// <summary>
    /// Exception thrown when tracking is not configured or has conflicting settings.
    /// </summary>
    public class TrackingConfigException : Exception
    {
        public TrackingConfigException(string message) 
            : base(message)
        {
        }
    }
}
