﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using PostSharp.Aspects;
using PostSharp.Aspects.Dependencies;

namespace DigiData.Tracking
{
    [Serializable]
    [ProvideAspectRole(StandardRoles.Tracing)]
    [AspectRoleDependency(AspectDependencyAction.Order,
        AspectDependencyPosition.After, StandardRoles.Security)]
    [AspectRoleDependency(AspectDependencyAction.Order,
        AspectDependencyPosition.After, StandardRoles.ExceptionHandling)]
    public sealed class TrackEventAttribute : MethodInterceptionAspect
    {
        public string EventName { get; set; }
        public double EventValue { get; set; }
        public string GroupName { get; set; }
        public string SessionID { get; set; }

        public override void CompileTimeInitialize(MethodBase method, AspectInfo aspectInfo)
        {
            if (String.IsNullOrEmpty(this.EventName))
            {
                this.EventName = String.Format("{0}.{1}",
                                               method.DeclaringType.Name,
                                               method.Name);
            }
        }

        public override void OnInvoke(MethodInterceptionArgs args)
        {
            if (string.IsNullOrEmpty(this.GroupName))
                TrackingUtilities.TrackEvent(this.EventName, this.EventValue, this.SessionID);
            else
                TrackingUtilities.TrackEvent(this.GroupName, this.EventName, this.EventValue, this.SessionID);

            args.Proceed();
        }
    }
}
