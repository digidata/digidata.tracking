﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using NLog;
using PostSharp.Aspects;
using PostSharp.Aspects.Dependencies;
using Trackerbird.Tracker;

namespace DigiData.Tracking
{
    [Serializable]
    [ProvideAspectRole(StandardRoles.Tracing)]
    [AspectRoleDependency(AspectDependencyAction.Order,
        AspectDependencyPosition.After, StandardRoles.Security)]
    [AspectRoleDependency(AspectDependencyAction.Order,
        AspectDependencyPosition.After, StandardRoles.ExceptionHandling)]
    public sealed class TrackingSetUpAttribute : MethodInterceptionAspect
    {
        static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public string TrackingUrl { get; set; }
        public string ProductID { get; set; }
        public string ProductBuildNumber { get; set; }
        public bool MultiSessionEnabled { get; set; }
        public string SessionID { get; set; }
        public bool AutoSyncEnabled { get; set; }

        public override void OnInvoke(MethodInterceptionArgs args)
        {
            TrackingUtilities.TrackingSetUp(this.TrackingUrl, this.ProductID,
                    this.ProductBuildNumber, this.MultiSessionEnabled,
                    this.SessionID, this.AutoSyncEnabled);

            args.Proceed();
        }
    }
}
