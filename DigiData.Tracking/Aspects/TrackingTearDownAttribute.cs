﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using NLog;
using PostSharp.Aspects;

namespace DigiData.Tracking
{
    [Serializable]
    public sealed class TrackingTearDownAttribute : MethodInterceptionAspect
    {
        static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public string SessionID { get; set; }

        public override void OnInvoke(MethodInterceptionArgs args)
        {
            try
            {
                args.Proceed();
            }
            finally
            {
                try
                {
                    TrackingUtilities.TrackingTearDown(this.SessionID);
                }
                catch (Exception e)
                {
                    Log.ErrorException("Error while tearing down tracking session", e);
                }
            }
        }
    }
}
