﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PostSharp.Aspects;
using PostSharp.Aspects.Dependencies;

namespace DigiData.Tracking
{
    [Serializable]
    [ProvideAspectRole(StandardRoles.Tracing)]
    [AspectRoleDependency(AspectDependencyAction.Order,
        AspectDependencyPosition.After, StandardRoles.Security)]
    [AspectRoleDependency(AspectDependencyAction.Order,
        AspectDependencyPosition.After, StandardRoles.ExceptionHandling)]
    public sealed class TrackTimedEventAttribute : OnMethodBoundaryAspect
    {
        private DateTime _start_time;

        public string EventName { get; set; }
        public string GroupName { get; set; }
        public string SessionID { get; set; }
        public string TimeUnit { get; set; }

        public override void CompileTimeInitialize(System.Reflection.MethodBase method, AspectInfo aspectInfo)
        {
            if (String.IsNullOrEmpty(this.EventName))
            {
                this.EventName = String.Format("{0}.{1}",
                                               method.DeclaringType.Name,
                                               method.Name);
            }
        }

        public override void OnEntry(PostSharp.Aspects.MethodExecutionArgs args)
        {
            _start_time = DateTime.Now;
        }

        public override void OnSuccess(MethodExecutionArgs args)
        {
            TimeSpan duration = DateTime.Now.Subtract(_start_time);
            double convertedTime = ConvertDuration(duration);

            string formattedEventName = String.Format("{0} ({1})",
                                                      this.EventName,
                                                      this.TimeUnit);

            if (string.IsNullOrEmpty(this.GroupName))
                TrackingUtilities.TrackEvent(formattedEventName, convertedTime);
            else
                TrackingUtilities.TrackEvent(this.GroupName, formattedEventName, convertedTime);
        }

        private double ConvertDuration(TimeSpan duration)
        {
            switch (TimeUnit)
            {
                case "ms":
                    return duration.TotalMilliseconds;
                case "s":
                    return duration.TotalSeconds;
                case "min":
                    return duration.TotalMinutes;
                default:
                    this.TimeUnit = "ms";
                    return duration.TotalMilliseconds;
            }
        }
    }
}
