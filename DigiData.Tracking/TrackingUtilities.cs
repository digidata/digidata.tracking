﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using NLog;
using Trackerbird.Tracker;
using PostSharp;

namespace DigiData.Tracking
{
    public class TrackingUtilities
    {
        static readonly Logger Log = LogManager.GetCurrentClassLogger();
        static ConcurrentQueue<Action> _queuedEvents = new ConcurrentQueue<Action>();

        public static void TrackingSetUp(string trackingUrl, string productID, string productBuildNumber, bool multiSessionEnabled, string sessionID, bool autoSyncEnabled)
        {
            if (!Trackerbird.Tracker.App.IsConfigLoaded)
            {
                if (TrackingConfig.IsTrackingSessionGlobal && !multiSessionEnabled)
                    throw new TrackingConfigException("The global session name is set but multisession is not enabled.");

                TBConfig config = new TBConfig(
                        trackingUrl, productID, TrackingConfig.Version,
                        productBuildNumber, multiSessionEnabled);

                config.SetFilePath(TrackingConfig.ConfigPath);
                config.SetProductLanguage(CultureInfo.CurrentUICulture.Name);

                Trackerbird.Tracker.App.Start(config);

                BackgroundWorker postStartBW = new BackgroundWorker();
                postStartBW.DoWork += WaitForTrackingStart;
                postStartBW.RunWorkerCompleted += ClearTrackingQueue;

                string session;
                if (multiSessionEnabled)
                {
                    if (TrackingConfig.IsTrackingSessionGlobal)
                        session = TrackingConfig.GlobalSessionID;
                    else
                    {
                        if (sessionID == null)
                            throw new ArgumentNullException("sessionID");
                        session = sessionID;
                    }

                    TrackingUtilities._queuedEvents.Enqueue(() => { Trackerbird.Tracker.App.SessionStart(session); });
                }

                if (autoSyncEnabled)
                    TrackingUtilities._queuedEvents.Enqueue(() => { Trackerbird.Tracker.App.StartAutoSync(); });

                postStartBW.RunWorkerAsync();
            }
        }

        private static void WaitForTrackingStart(object sender, DoWorkEventArgs e)
        {
            while (Trackerbird.Tracker.App.StartStatus == StartStatusEnum.Loading)
            {
                Thread.Sleep(10);
            }
        }

        private static void ClearTrackingQueue(object sender, RunWorkerCompletedEventArgs e)
        {
            Action queuedAction;

            while (_queuedEvents.TryDequeue(out queuedAction))
            {
                try
                {
                    queuedAction();
                }
                catch (Exception ex)
                {
                    Log.ErrorException("Error while clearing queue", ex);
                }
            }
        }

        public static void TrackingTearDown(string sessionID)
        {
            if (TrackingConfig.IsTrackingSessionGlobal)
                Trackerbird.Tracker.App.SessionStop(TrackingConfig.GlobalSessionID);
            else if (!String.IsNullOrEmpty(sessionID))
                Trackerbird.Tracker.App.SessionStop(sessionID);

            Trackerbird.Tracker.App.Stop();
        }

        public static void TrackEvent(string eventName, double? eventValue, string sessionID = null)
        {
            if (TrackingConfig.TrackingEnabled)
            {
                if (TrackingConfig.IsTrackingSessionGlobal)
                {
                    ProcessTrackEvent(() => {
                        Trackerbird.Tracker.App.EventTrack(eventName, eventValue, TrackingConfig.GlobalSessionID);
                    });
                }
                else if (!String.IsNullOrEmpty(sessionID))
                {
                    ProcessTrackEvent(() => {
                        Trackerbird.Tracker.App.EventTrack(eventName, eventValue, sessionID);
                    });
                }
                else
                {
                    ProcessTrackEvent(() => {
                        Trackerbird.Tracker.App.EventTrack(eventName, eventValue);
                    });
                }
            }
        }

        public static void TrackEvent(string groupName, string eventName, double? eventValue = null, string sessionID = null)
        {
            if (TrackingConfig.TrackingEnabled)
            {
                if (TrackingConfig.IsTrackingSessionGlobal)
                {
                    ProcessTrackEvent(() =>
                    {
                        Trackerbird.Tracker.App.EventTrack(groupName, eventName, eventValue, TrackingConfig.GlobalSessionID);
                    });
                }
                else if (!String.IsNullOrEmpty(sessionID))
                {
                    ProcessTrackEvent(() =>
                    {
                        Trackerbird.Tracker.App.EventTrack(groupName, eventName, eventValue, sessionID);
                    });
                }
                else
                {
                    ProcessTrackEvent(() =>
                    {
                        Trackerbird.Tracker.App.EventTrack(groupName, eventName, eventValue);
                    });
                }
            }
        }

        public static void TrackException(string className, string methodName, Exception exception)
        {
            if (TrackingConfig.TrackingEnabled)
            {
                ProcessTrackEvent(() =>
                {
                    Trackerbird.Tracker.App.ExceptionTrack(className, methodName, exception);
                });
            }
        }

        private static void ProcessTrackEvent(Action trackEvent)
        {
            if (IsTrackingLoading)
                TrackingUtilities._queuedEvents.Enqueue(trackEvent);
            else
                trackEvent();
        }

        public static bool IsTrackerRunning
        {
            get { return Trackerbird.Tracker.App.StartStatus == StartStatusEnum.OK; }
        }

        public static bool IsTrackingLoading
        {
            get { return Trackerbird.Tracker.App.StartStatus == StartStatusEnum.Loading; }
        }

        public static bool HasQueuedEvents
        {
            get { return !_queuedEvents.IsEmpty; }
        }

        /// <summary>
        /// Gets a boolean that determines whether PostSharp has hit the calling assembly.
        /// </summary>
        public static bool IsTransformedByPostSharp
        {
            get { return Post.IsTransformed; }
        }
    }
}
